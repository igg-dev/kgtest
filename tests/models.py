# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.db.models.signals import post_save
from django.dispatch import receiver

class Subject(models.Model):
  name = models.TextField( _(u'название'),max_length=255)

  class Meta:
    verbose_name = _(u'Предмет')
    verbose_name_plural = _(u'Предметы')

  def __unicode__(self):
      return unicode(self.name)

class Topic(models.Model):
  name = models.TextField( _(u'название'),max_length=255)
  subject = models.ForeignKey(Subject, related_name='topics'  ,on_delete=models.CASCADE)
  parentId = models.ForeignKey("self", default=None, blank=True, null=True)

  def __unicode__(self):
      return unicode(self.name)
      
  class Meta:
    verbose_name = _(u'Тема')
    verbose_name_plural = _(u'Темы')

class Question(models.Model):
    content = models.TextField( _(u'текст вопроса'))
    subject = models.ForeignKey(Subject, related_name="questions", on_delete=models.CASCADE)
    topic = models.ForeignKey(Topic, related_name="questions",on_delete=models.CASCADE)
    
    class Meta:
        verbose_name = _(u'Вопрос')
        verbose_name_plural = _(u'Вопросы')


class Answer(models.Model):
  content = models.CharField(_(u'текст'),max_length=255 )
  true_answer = models.BooleanField(_(u'правильный'),default=False)
  question = models.ForeignKey(Question, related_name="answers", on_delete=models.CASCADE)

  class Meta:
      verbose_name = _(u'Ответ')
      verbose_name_plural = _(u'Ответы')

class Student(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    patronymic = models.CharField(max_length=20, blank=True, default=None, null=True)
    location = models.CharField(max_length=30, blank=True)
    school = models.CharField(max_length=100, null=True, blank=True)      
    grade = models.CharField(max_length=100, null=True, blank=True)     

@receiver(post_save, sender=User)
def create_user_student(sender, instance, created, **kwargs):
    if created:
        Student.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_student(sender, instance, **kwargs):
    if hasattr(instance, 'student'):
        instance.student.save()
