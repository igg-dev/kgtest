from django.conf.urls import url, include
from django.contrib.auth import views as auth_views
from . import views

urlpatterns = [
    url(r'^tests/$', views.tests_index, name='tests_index'),
    url(r'^subject/$', views.subject_page, name='subject_page'),
    url(r'^registration/$', views.student_registration, name='student_registration'),
    url(r'^login/$', views.login_student, name='student_login'),
    url(r'^logout/$', auth_views.logout, {'template_name': 'tests/logged_out.html'},name='logout'),
    url(r'^student_profile/$', views.student_profile, name='student_profile'),
    url(r'^api/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api/subjects/$', views.subjects, name='api_subjects'),
    url(r'^api/test/answer/(?P<id>[0-9]*)/$', views.answer_test, name='api_answer_test'),
    url(r'^api/test/(?P<id>[0-9]*)/$', views.start_test, name='api_test'),
    url(r'^$', views.main_page, name='main_page'),
] 