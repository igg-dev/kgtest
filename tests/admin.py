# -*- coding: UTF-8 -*-
from django.contrib import admin
from django.contrib.auth.models import User
from models import Question, Answer, Subject, Topic, Student
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from django.utils.text import force_text
import nested_admin

# class AnswerInline(admin.StackedInline):
#   model= Answer  
#   extra = 0



# class QuestionAdmin(admin.ModelAdmin):
#   save_on_top = True
#   list_display = ('id', 'content' )
#   inlines = [
#     AnswerInline,
#   ]



# class QuestionInline(admin.StackedInline):
#   model= Question  
#   extra = 0
#   fields = ["get_edit_link", "content"]
#   readonly_fields = ["get_edit_link"]

#   def get_edit_link(self, obj=None):
#       if obj.pk:  # if object has already been saved and has a primary key, show link to it
#           url = reverse('admin:%s_%s_change' % (obj._meta.app_label, obj._meta.model_name), args=[force_text(obj.pk)])
#           return """<a href="{url}">{text}</a>""".format(
#               url=url,
#               text="Edit this %s separately" % obj._meta.verbose_name,
#           )
#       return _("(save and continue editing to create a link)")
#   get_edit_link.short_description = _("Edit link")
#   get_edit_link.allow_tags = True
  
# class TopicAdmin(admin.ModelAdmin):
#   save_on_top = True
#   list_display = ('id', 'name' )
#   inlines = [
#     QuestionInline,
#   ]

class AnswerInline(nested_admin.NestedTabularInline):
    model = Answer
    extra = 0

class QuestionInline(nested_admin.NestedTabularInline):
    model = Question
    extra = 0
    inlines = [AnswerInline]

class TopicAdmin(nested_admin.NestedModelAdmin):
    save_on_top = True
    inlines = [QuestionInline]

class SubjectAdmin(admin.ModelAdmin):
  save_on_top = True
  list_display = ('id', 'name' )

class StudentAdmin(admin.ModelAdmin):
  list_display = ('id', 'location', 'school' )

# admin.site.register(Question, QuestionAdmin) 
admin.site.register(Subject, SubjectAdmin) 
admin.site.register(Topic, TopicAdmin) 
admin.site.register(Student, StudentAdmin) 
