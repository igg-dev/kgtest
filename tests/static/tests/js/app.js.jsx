var React = require('react');
var ReactDOM = require('react-dom');
var Button = require('react-bootstrap').Button;
var Modal = require('react-bootstrap').Modal;
var Form = require('react-bootstrap').Form;
var OverlayTrigger  = require('react-bootstrap').OverlayTrigger ;

function getCookie(name) {
  var cookieValue = null;
  if (document.cookie && document.cookie != '') {
    var cookies = document.cookie.split(';');
    for (var i = 0; i < cookies.length; i++) {
        var cookie = jQuery.trim(cookies[i]);
        // Does this cookie string begin with the name we want?
        if (cookie.substring(0, name.length + 1) == (name + '=')) {
            cookieValue = decodeURIComponent(
                cookie.substring(name.length + 1)
            );
            break;
        }
    }
  }
  return cookieValue;
}

var subjectsUrl="/api/subjects/";
var questionsUrl="/api/test/";
var loginUrl="/login/"

export default class DjangoCSRFToken extends React.Component {
    render(){
        var csrf_token = getCookie('csrftoken');
        return (<input type="hidden" name="csrfmiddlewaretoken" { ...this.props }/>);
    }
}

const TestButton = React.createClass({
  getInitialState() {
    return { 
      showModal: false,
      question_ind: 0,
      question: {},
      questions: [],
      answers: [],
      button_text: "Следующий вопрос"
    };
  },

  close() {
    this.setState({ showModal: false });
  },

  fetch_questions(){
    $.ajax({
      url: questionsUrl+this.props.test,
      dataType: 'json',
      cache: false,
      success: function(data) {
        this.setState({
          questions: data,
          question: data[0],
          answers: data[0].answers
        }); 
      }.bind(this),
      error: function(xhr, status, err) {
        this.setState({ showModal: false})
        window.location.href = loginUrl;
      }.bind(this)
    });
    
  },

  open(e) {
    e.preventDefault()
    this.setState({ showModal: true })
    this.fetch_questions();
  },

  next_question(){
    var index = this.state.question_ind;
    console.log(index)
    if (index==this.state.questions.length-2){
      this.setState({
        button_text: "Завершить"
      })
    }
    else if (index>=this.state.questions.length-1){
      this.setState({
        showModal: false
      })
    }
    $.ajax({
      type: "POST",
      url: '/api/test/answer/'+this.state.questions[index].id+'/', 
      data: $(this.refs.form).serialize(),
      dataType: 'json',
      success: function(data) {
        index+=1;
        console.log(this.state.questions[index])
        this.setState({
          question_ind: index,
          question: this.state.questions[index],
          answers: this.state.questions[index].answers
        })
      }.bind(this),
      error: function(xhr, status, err) {
        this.setState({ showModal: false})
        window.location.href = loginUrl;
      }.bind(this)
    });
  },

  render() {
    var answersTemplate
    var csrf_token = getCookie('csrftoken');
    if (this.state.answers.length > 0) {
      answersTemplate = this.state.answers.map(function(item, index) {
        return (
            <p key={index}> 
           <label>
              <input type="radio" name="answer[id]" value={item.id} id="answerRadio"/>
              {item.content} 
              </label>
            </p>
        )}
    )}

    return (
      <div>
          <Button
          bsStyle="primary"
          bsSize="small"
          onClick={this.open}
          className="startTest">
          Начать
          </Button>

        <Modal show={this.state.showModal} onHide={this.close} bsSize="large" aria-labelledby="contained-modal-title-lg">
         <form ref="form">
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-bg">{this.props.name} вопрос {this.state.question_ind+1} из {this.state.questions.length}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <DjangoCSRFToken value={csrf_token}/>
            <div>{this.state.question.content}</div>
              <input type="hidden" value={this.state.question.id} name="question[id]" />
               <div className="Answers">
                  {answersTemplate}  
               </div>
          </Modal.Body>
          <Modal.Footer>
             <Button onClick={this.next_question}>{this.state.button_text}</Button>
          </Modal.Footer>
         </form>
        </Modal>
      </div>
    );
  }
});
const Topics = React.createClass({
  getInitialState() {
    return { 
   
       };
  },
  render(){
      var sbjc = this.props.subject_id
      
      var tpcs = this.props.topics.map(function(item, index){
        return (
          <li key={index}>
             <span href="#">
                <div className="test-container">
                    <div className="circle">
                        <div className="star"></div>
                    </div>
                    <div className="testName">{item.name}
                        <TestButton test={item.id} name={item.name}/>
                    </div>
                </div>
              </span>
          </li>
        )
      })
    return(
        <ul className="topic list">
          {tpcs}
        </ul>
      )
  }
});


const Subjects = React.createClass({
  getInitialState() {
    return { 
          data: []   
       };
  },
  componentDidMount(){
    $.ajax({
       url: subjectsUrl,
       dataType: 'json',
       cache: false,
       success: function(data) {
         this.setState({data: data}); // Notice this
       }.bind(this),
       error: function(xhr, status, err) {

       }.bind(this)
     });
  },

  render(){
    // var tpcs=this.props.data.topics
    var sbj = this.state.data.map(function(subject,index){
      return(
         <li key={index}>
              <div id="subj_name">
                  <div className="theme-top" id="subject">
                     <a href="#" id={index}> {subject.name}</a>
                  </div>
                  <Topics topics={subject.topics} subject_id={subject.id}/>
              </div>
        </li>
      )
    })

    var cont = this.state.data.map(function(subject,index){
      return(
         <li key={index}>
               <a href={`#${index}`}>{subject.name}</a>
        </li>
      )
    })
    return(
      <div className="row">
      <div className="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <div className="contents-container">
            <h3>Содержание</h3>
            <ul className="contents scroll-menu" id="scroll-menu" role="menu">
            {cont}

            </ul>
          </div>
        </div>
      <div id="subjects-root" className="subject-content col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <ul className="themes"  role="menu">
            {sbj}
        </ul>
        </div>
       </div> 
      )
  }
});



ReactDOM.render(<Subjects />, 
  document.getElementById('test-content'));







