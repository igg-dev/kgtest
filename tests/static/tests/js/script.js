// require('bootstrap')
$( document ).ready(function() {
    
    $('.dropdown-toggle').dropdown()
    $('li.dropdown').on('show.bs.dropdown', function(){
	       $('nav.navbar-custom').addClass('nav-white')
    }).on('hide.bs.dropdown', function(){
	   $('nav.navbar-custom').removeClass('nav-white')
    })
// $('.subjects div h2 a').click(function(e)
// {
// 	e.preventDefault();
// 	var par = $(this).parents('.row')[0]
// 	$(par).find('ul.list').slideToggle("fast");
// }
// 	)

// $('.dropdown-menu .menu-list-par a .fullwidth').click(function(e)
// {
// 	e.preventDefault();
// 	var par = $(this).parents('.menu-list-par')[0]
// 	$('.menu-list-par').not(par).find('ul.menu-list').hide();
// 	$(par).find('ul.menu-list').slideToggle("fast");
// }
// 	)

// $(function () {       
//  var a = $(".contents-container"),
//             c = $(".mathpage-content").offset();
//             d = a.outerHeight(!0),
//             e = $("footer").offset();
//         $(document).scroll(function () {
//             var b = $(this).scrollTop(),
//                 b = e.top - (b + d + c.top);
//             0 < b ? a.css({
//                 background: "red",
//                 top: c.top
//             }) : a.css({
//                background: "green",
//                 top: c.top + b
//             })
//         })
//     });

    $(window).scroll(function() {
			if ($(this).scrollTop() > 250) {
                $('.subject-container .contents-container').addClass('fixed');
		    } else {
		        $('.subject-container .contents-container').removeClass('fixed');
		    }
	});


    $(".scroll-menu").on("click","a", function (event) {
        event.preventDefault();
        var id  = $(this).attr('href'),
        top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 500);
    });

$('#scrollup img').mouseover( function(){
        $( this ).animate({opacity: 0.65},100);
        
    }).mouseout( function(){
        $( this ).animate({opacity: 1},100);
    }).click( function(){
        window.scroll(0 ,0); 
        $('body,html').animate({scrollTop: top}, 500);
        return false;
    });

    // $("#scrollup img").on("click","a", function (event) {
    //     event.preventDefault();
    //     var id  = $(this).attr('href'),
    //     top = 0px;
    //     $('body,html').animate({scrollTop: top}, 500);
    // });

    $(window).scroll(function(){
        if ( $(document).scrollTop() > 0 ) {
            $('#scrollup').fadeIn('fast');
        } else {
            $('#scrollup').fadeOut('fast');
        }
    });

    $('.c100').addClass(function(){
        var right = parseInt($(this).data('true'));
        var total = parseInt($(this).data('total'));
        var percent = Math.round((right/total)*100);
        var color;
        console.log(percent)
        $(this).find('span').text(percent+'%');

       $(this).siblings('.rightAnswers').text(right+' из '+total) 

         if(percent > 50){
            $(this).siblings('.isPass').addClass('passed').find('span').text("Пройден");
             }
         else{
            $(this).siblings('.isPass').addClass('notPassed').find('span').text("Не пройден");
             }

             if(percent < 50){ color="red"; }
             else if(percent < 75){ color="blue"; }
             else{ color="green"; }

          return 'p'+percent+ '  '+color;
    })

    $(window).scroll(function() {
            if ($(this).scrollTop() > 350) {
                $('.table-head').addClass('fixedTableHead');
            } else {
                $('.table-head').removeClass('fixedTableHead');
            }
    });

      $.validate({
       lang : 'ru'
    });

  $.validate({
  modules : 'security',
  onModulesLoaded : function() {
    var optionalConfig = {
      fontSize: '12pt',
      padding: '4px',
      bad : 'Very bad',
      weak : 'Weak',
      good : 'Good',
      strong : 'Strong'
    };

    $('input[name="pass"]').displayPasswordStrength(optionalConfig);
  }
});
});
