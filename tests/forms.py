# -*- coding: utf-8 -*-

from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import Student


class UserForm(forms.ModelForm):
    last_name = forms.CharField(label='Фамилия', max_length=20)
    first_name = forms.CharField(label='Имя', max_length=20)
    # username = forms.CharField(label='Отчество', max_length=20)
    password = forms.CharField(label='Пароль',widget=forms.PasswordInput)

    def save(self, commit=True):
        user = super(UserForm, self).save(commit=False)
        user.username = self.cleaned_data["last_name"]+self.cleaned_data["first_name"]
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'password')

class RegistrationFormStudent(forms.ModelForm):
    patronymic = forms.CharField(label='Отчество', max_length=20, required=False)
    location = forms.CharField(label='Населенный пункт', max_length=100)
    school = forms.CharField(label='Школа', max_length=100)
    grade = forms.CharField(label='Класс', max_length=100)


    class Meta:
        model = Student
        fields = ('patronymic', 'location', 'school', 'grade',)




