from .models import Subject, Question, Answer, Topic, Student
from rest_framework import serializers


class TopicSerializer(serializers.ModelSerializer):
    # subject = serializers.SerializerMethodField()
    name= serializers.CharField(required= False, read_only = True)

    class Meta:
        model = Topic
        fields = ('id', 'name')

    # def get_subject(self, obj):
    #   return obj.context.subject

class SubjectSerializer(serializers.ModelSerializer):
  topics = TopicSerializer(many = True)
  name = serializers.CharField(read_only = True)

  class Meta:
    model = Subject
    fields = ('id', 'name', 'topics')

class AnswerSerializer(serializers.ModelSerializer):
    # subject = serializers.SerializerMethodField()
    content= serializers.CharField(required= False, read_only = True)

    class Meta:
        model = Answer
        fields = ('id', 'content')

class QuestionSerializer(serializers.ModelSerializer):
  answers = AnswerSerializer(many = True)
  content = serializers.CharField(read_only = True)

  class Meta:
    model = Question
    fields = ('id', 'content', 'answers')

