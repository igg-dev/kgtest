from collections import defaultdict
from django.db import transaction
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from .models import Subject, Question, Answer, Topic, Student
from .forms import RegistrationFormStudent, UserForm
from rest_framework.response import Response
from rest_framework.decorators import api_view, renderer_classes, authentication_classes, permission_classes
from rest_framework.renderers import JSONRenderer
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from .serializers import TopicSerializer, SubjectSerializer, AnswerSerializer, QuestionSerializer
import pdb
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse


@api_view(['GET'])
@renderer_classes([JSONRenderer])
def subjects(request):
    subjects = Subject.objects.all()
    serializer = SubjectSerializer(subjects, many=True)
    return Response(serializer.data)

@api_view(['GET'])
@renderer_classes([JSONRenderer])
@authentication_classes([SessionAuthentication])
@permission_classes((IsAuthenticated, ))
def start_test(request, id):
    questions = Topic.objects.get(pk=id).questions
    serializer = QuestionSerializer(questions, many=True)
    return Response(serializer.data)

@api_view(['POST'])
@renderer_classes([JSONRenderer])
@authentication_classes([SessionAuthentication])
@permission_classes((IsAuthenticated, ))
def answer_test(request, id):
    answer = Answer.objects.get(pk=id)
    return Response({'status': 200})


def main_page(request):
    return render(request, 'tests/main_page.html', {})

def subject_page(request):
    context = {'message': "You are at the subject page"}
    return render(request, 'tests/subject.html', context)

def tests_index(request):
    subjects = Subject.objects.all()
    topics = Topic.objects.all()
    topics_grouped = defaultdict(list)

    for topic in topics:
        topics_grouped[topic.subject_id].append(topic)
        
    topics_by_subject= dict(topics_grouped)


    return render(request, 'tests/tests_index.html', {"subjects": subjects, "topics_by_subject": topics_by_subject })

@transaction.atomic
def student_registration(request):
    if request.method == 'POST':
        user_form = UserForm(request.POST)
        student_form = RegistrationFormStudent(request.POST)
        if user_form.is_valid() and student_form.is_valid():
            user = user_form.save()
            user.refresh_from_db()  # This will load the Student created by the Signal
            student_form = RegistrationFormStudent(request.POST, instance=user.student)  # Reload the student form with the student instance
            student_form.full_clean()  # Manually clean the form this time. It is implicitly called by "is_valid()" method
            student_form.save()
            # pdb.set_trace()
            raw_password = user_form.cleaned_data.get('password')
            user_ = authenticate(username=user.username, password=raw_password)
            login(request, user_)
            return redirect('tests_index')
        else:
            return HttpResponse('Form is not valid')

    # if a GET (or any other method) we'll create a blank form
    else:
        user_form = UserForm()
        student_form = RegistrationFormStudent()

    return render(request, 'tests/registration.html', {
        'user_form': user_form,
        'student_form': student_form
    })


@csrf_exempt
def login_student(request):
    # if request.user.is_authenticated:
    #     # return redirect('tests_index')
    #     return render(request, 'tests/tests_index.html', {})
    # else:
        logout(request)
        # pdb.set_trace()
        username = password = ''
        if request.POST:
            username = request.POST['username']
            password = request.POST['password']

            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return redirect('tests_index')
        return render(request, 'tests/login_student.html', {})


@login_required(login_url='/login/')
def student_profile(request):
    return render(request, 'tests/student_profile.html', {})
    # return HttpResponse('Student Profile')
