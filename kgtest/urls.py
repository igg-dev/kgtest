from django.conf.urls import include, url
from django.contrib import admin
# from tests import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    # url(r'^tests/', include('tests.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'', include('tests.urls')),
    url(r'^nested_admin/', include('nested_admin.urls')),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
